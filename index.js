const puppeteer = require('puppeteer-core');

const TIMEOUT_LIMIT = 60000;
let timeoutInstance = null;

async function main() {
    let compraUrl = 'https://compragamer.com/index.php?criterio=3080&x=0&y=0&seccion=3&nro_max=50';
    // let compraUrl = 'https://compragamer.com/index.php?seccion=3&cate=78&nro_max=50';

    let browser = await puppeteer.launch({
        headless: false,
        // Path al ejecutable de Chrome
        executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        // Path al userInfo cache de Chrome del usuario (macOS)
        userDataDir: '/Users/<username>/Library/Application Support/Google/Chrome/Default',
        args: ['--no-sandbox']
        // devtools: true
    });

    let page = await browser.newPage();

    page.setDefaultTimeout(0);

    await page.goto(compraUrl, {
        waitUntil: 'networkidle2'
    });

    // await page.waitForNavigation();
    await page.waitForSelector('.sorting__info');

    let sortingData = await page.evaluate(() => {
        let searchResults = document.querySelector('span[class="sorting__info"').innerText;

        return {
            searchResults
        }
    });

    if (sortingData.searchResults.indexOf('Mostrando 1 - 1 de 1 Items') > -1) {
        console.log('!!!!!Hay STOCK de 3080!!!!');

        // Hacer click en agregar al carrito
        const addToCartButton = '.products-btns__btn.products-btns__add';
        await page.waitForSelector(addToCartButton);
        await page.click(addToCartButton);

        // await page.waitForNavigation();

        // Set Codigo Postal
        await page.waitForSelector('.products__name');

        const cpInput = await page.$('#cp');
        await cpInput.click({ clickCount: 3 });
        await page.keyboard.type('1414');

        await page.waitForSelector('#ingresa_cp');
        const cpButton = await page.$$('#ingresa_cp');
        await cpButton[0].click();

        await page.waitForNavigation();

        // Click en "Comprar en un pago"
        await page.waitForSelector('.card-btns__add');
        const comprarEnPagoButton = await page.$$('.card-btns__add');
        await comprarEnPagoButton[0].click();

        await page.waitForNavigation();

        // Select shipping type -- STEP 1 of 6

        await page.select('select[name="forma_de_envio"]', '20');

        const next1of6Button = await page.$x("//input[@type='submit']");
        await next1of6Button[0].click();

        await page.waitForNavigation();
        await page.waitForSelector('.products__name');

        // Select armado pedido -- STEP 2 of 6

        const alreadyCustomerCheckbox = await page.$x("//input[@type='radio']");
        await alreadyCustomerCheckbox[1].click();

        const next2of6Button = await page.$x("//input[@value='siguiente']");
        await next2of6Button[0].click();

        await page.waitForNavigation();

        // Select información personal -- STEP 3 of 6

        // SET NAME
        await page.$eval('input[name=nombre]', el => el.value = 'Nombre');

        // SET LASTNAME
        await page.$eval('input[name=apellido]', el => el.value = 'Apellido');

        // SET NR DOC
        await page.$eval('input[name=nr_doc]', el => el.value = 'DNI');

        // SET PROVINCIA { 1: Capital Federal, 2: Buenos Aires }
        await page.select('select[name="id_prov"]', '1');

        // SET TELEFONO TYPE
        const telephoneSelect = await page.$x("//input[@type='radio']");
        await telephoneSelect[1].click();

        // SET NRO TELEFONO
        await page.$eval('input[name=nuevo_te]', el => el.value = 'TELEFONO');

        // SET EMAIL
        await page.$eval('input[name=nuevo_mail]', el => el.value = 'johndoe@hotmail.com');

        const next3of6Button = await page.$x("//input[@value='Siguiente']");
        await next3of6Button[0].click();

        await page.waitForNavigation();

        // Select direccion de entrega -- STEP 4 of 6

        // SET CALLE
        await page.$eval('input[name=envios_calle]', el => el.value = 'Siempre Vivas');

        // SET ALTURA
        await page.$eval('input[name=envios_altura]', el => el.value = '800');

        // SET PISO
        await page.$eval('input[name=envios_piso]', el => el.value = '8');

        // SET DEPTO
        await page.$eval('input[name=envios_dto]', el => el.value = 'A');

        // SET ENTRE CALLES
        await page.$eval('input[name=envios_entrecalles]', el => el.value = 'Juncal y Billingburst');

        // SET LOCALIDAD
        await page.select('select[name="envios_loc"]', 'CABA');

        // SET PROV
        await page.select('select[name="envios_prov"]', '1');

        const next4of6Button = await page.$x("//input[@value='Siguiente']");
        await next4of6Button[0].click();

        await page.waitForNavigation();

        // Select confirmar datos -- STEP 5 of 6

        const next5of6Button = await page.$x("//input[@name='fact_ok']");
        await next5of6Button[0].click();

        console.log('3080 ACQUIRED!!! :D ');

        clearTimeout(timeoutInstance);
    } else {
        console.log('No hay stock.... :(');
        await browser.close();

        timeoutInstance = setTimeout(main, TIMEOUT_LIMIT);
        console.log('↗ Intentando buscar stock de nuevo...');
    }
}

try {
	void main();
} catch (error) {
	// Ignoring errors; more than likely due to rate limits
	console.log(error);
	void main();
}